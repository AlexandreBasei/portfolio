import React from 'react';
import './css/Footer.css';

function Footer() {
  return (
    <footer>
      <p>alexandreacevedobasei@gmail.com</p>
      <p>06 46 88 46 88</p>
      <p>© 2024 Alexandre Basei. Tous droits réservés.</p>
    </footer>
  );
}

export default Footer;