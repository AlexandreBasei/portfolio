import React from 'react';
import './css/Topfood.css';
import phoneMockup from './images/png/mobile-mockup.png';
import arobase from './images/svg/arobase.svg';
import cloche from './images/svg/cloche.svg';
import coeur from './images/svg/coeur.svg';
import feu from './images/svg/feu.svg';
import monde from './images/svg/monde.svg';
import photo from './images/svg/photo.svg';
import pin from './images/svg/pin.svg';
import search from './images/svg/search.svg';
import stats from './images/svg/stats.svg';
import user from './images/svg/user.svg';

function Topfood() {

    return (
        <section id='topfood'>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
                <div>
                    <img src={arobase} alt="arobase" />
                    <img src={cloche} alt="arobase" />
                    <img src={coeur} alt="arobase" />
                    <img src={feu} alt="arobase" />
                    <img src={monde} alt="arobase" />
                    <img src={photo} alt="arobase" />
                    <img src={pin} alt="arobase" />
                    <img src={search} alt="arobase" />
                    <img src={stats} alt="arobase" />
                    <img src={user} alt="arobase" />
                </div>
            </div>
            <div className="animated-title">
                <div className="text-top">
                    <div>
                        <span>Topfood</span>
                    </div>
                </div>
            </div>

            <div className='descr'>
                <p>TopFood est un réseau social centré sur la nourriture, qui vous permet de partager vos restaurants préférés en fonction des plats que vous aimez. Découvrez des restaurants et aiguisez votre palais en analysant vos goûts personnels !</p>

                <div>
                    <button className='download'>Télécharger l'application TopFood</button>
                    <button className='download'>Visiter TopFood</button>
                </div>
            </div>

            <img src={phoneMockup} alt="Projet Topfood" />
        </section>
    );
}

export default Topfood;