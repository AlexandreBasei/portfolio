import React from 'react';
import './css/Game.css';
import vrMockup from './images/png/vr-mockup.png';
import sparkles from './images/svg/sparkles.svg';
import cube from './images/svg/cube.svg';
import oeil from './images/svg/oeil.svg';
import music from './images/svg/music.svg';
import lamp from './images/svg/lamp.svg';
import rocket from './images/svg/rocket.svg';
import puzzle from './images/svg/puzzle.svg';
import couches from './images/svg/couches.svg';
import hand from './images/svg/hand.svg';
import clock from './images/svg/clock.svg';


function Game() {

    return (
        <section id='game'>

            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>
            <div className='row'>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
                <div>
                    <img src={sparkles} alt="sparkles" />
                    <img src={cube} alt="sparkles" />
                    <img src={oeil} alt="sparkles" />
                    <img src={music} alt="sparkles" />
                    <img src={lamp} alt="sparkles" />
                    <img src={rocket} alt="sparkles" />
                    <img src={puzzle} alt="sparkles" />
                    <img src={couches} alt="sparkles" />
                    <img src={hand} alt="sparkles" />
                    <img src={clock} alt="sparkles" />
                </div>
            </div>

            <div className="animated-title">
                <div className="text-top">
                    <div>
                        <span>Thumanite VR</span>
                    </div>
                </div>
            </div>

            <div className='descr'>
                <p>Thumanite est une expérience interactive en réalité virtuelle qui propose un voyage à travers différents époques marquantes de l'Histoire, chacune proposant une énigme unique à résoudre. Embarquez dans un voyage unique sur les traces de l'humanité !</p>

                <button className='download'>Regarder la démonstration</button>
            </div>


            <img src={vrMockup} alt="Projet Topfood" />
        </section>
    );
}

export default Game;